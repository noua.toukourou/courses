# Problem solving
A guide for solving computing issues

1. Express the problem 
  * Write down what you want to achieve
2. Search for help 
  * Read **FAQs**, **help pages** and the **official documentation** well before turning to Google
  * Use stack exchange, forums and related resources carefully
3. Ask an expert 



# Problem solving
## Write to the Oracle 

  * The Oracle gives the precise answer to your problems
    * You have to submit the problem in writing
    * The Oracle answers a questions only once or if it finds the problem interesting
    * If you supply a trivial problem, it will stop answering
    
  * Available Oracles
    * Service Now @ [service.uni.lu] (Uni and LCSB helpdesk)
    * [Stack Overflow](https://stackoverflow.com/) and other online sites
    * Local experts
