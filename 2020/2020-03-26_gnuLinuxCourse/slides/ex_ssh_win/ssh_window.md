# SSH Window
<div class="multicol">

<div class="col" data-markdown>

## Important Fields

1. Hostname, or the IP address of the server
2. Username associated with your account on the server
3. Server-side SSH port
4. If key-based authentication, provide your private key here

<!-- .element: class="fragment" data-fragment-index=1 -->

1. `linuxcourse-test.lcsb.uni.lu`, or 10.240.16.69
2. Your username
3. 8022 (if we managed to change that :) )
4. Nothing yet

<!-- .element: class="fragment" data-fragment-index=2 -->

</div>

<div class="col" data-markdown>

![][sshwindow]

</div>


</div>


[sshwindow]: variae/figs/mxt_shots/mobaxterm_ssh_interactive.png "MobaXterm SSH setup"

