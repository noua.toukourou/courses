# The Barrier between the Human and the Machine
<div class="multicol">

<div class="col" data-markdown>

* What is a shell?
  - Omnipresent user interface
  
  <!-- .element: class="fragment"  -->
  - It interprets commands given by the user
  
  <!-- .element: class="fragment"  -->
  - It is heavily keyboard based
  
  <!-- .element: class="fragment"  -->

<!-- .element: class="fragment"  -->

* Shell runs within a terminal application
	- In ancient times, these were actual pieces of hardware
	
	<!-- .element: class="fragment"  -->
	- Today, a program that echoes user input and displays command results
	  
	<!-- .element: class="fragment"  -->

<!-- .element: class="fragment" -->
	  
* How to access it?
  - **GNU/Linux systems:**
    - Depends on the desktop interface (of which there are many)
	- Access from a `Start`-like menu, or a `Win`-key search as
      *terminal*, or *console*
	- if all else fails, `Ctrl` + `Alt` + `F[2-6]`
	
  <!-- .element: class="fragment"  -->
  - **Windows:** [MobaXterm][1] which you hopefully installed
  
  <!-- .element: class="fragment"  -->
  - **Mac:** Built-in terminal; *Spotlight* search for *terminal*
  
  <!-- .element: class="fragment"  -->
	
<!-- .element: class="fragment" -->

</div>

<div class="col" data-markdown>

<img src="variae/figs/shell/3270.jpg" width="90%" />

<!-- .element: class="fragment"  -->

</div>

</div>


[terminal]: variae/figs/shell/3270.jpg "IBM 3270"

[1]: https://mobaxterm.mobatek.net/download-home-edition.html

