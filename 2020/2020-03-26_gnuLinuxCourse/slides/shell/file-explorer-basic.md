# File Explorer: Basics
```console
$ pwd    # Displays the absolute path to the current dirrectory.
$ ls     # List contents of the current directory.
$ mkdir linux-practical-01    # Create a new directory.
$ cd linux-practical-01    # Switch to a different directory.
$ touch pr1-hello-world.txt     # Create a new and empty file.
$ mkdir subdir-01 # Create another directory.
$ touch subdir-01/a subdir-01/b subdir-01/c    # Create multiple empty files.
$ ls subdir-01    # List contents of directory.
$ mkdir -p subdir-02/dir-X/dir-Y/dir-Z    # Create a nested directory under the path.
$ ls subdir-02/dir-X # Confirm the directory dir-Y has been created.
$ history    # Lists the commands executed so far.
$ cd
```

<!-- * Concepts -->
<!--   - Files and directories -->
<!--   - Paths -->
<!--   - Importance of quoting -->
<!--   - History -->
